public class Persona {
    private String nombre;
    private int edad;
	private int anio;
    private String sexo;
    private double peso, altura;
    
    
    public Persona(String nombre, int edad, String sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        altura = altura;
    }

    public void setEdad(int edad) {
        edad = edad;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
       this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        sexo = sexo;
    }
	
	public int getAnio(){
		return anio;
	}
	
	public void setAnio(int sanio){
		anio = sanio;
	}
	
	
	public int getEdad(){
		
		return edad;
	}

    

}
