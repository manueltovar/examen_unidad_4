import java.util.Scanner;

public class PruebaPersona{
	
	public static void main(String[]args){
	
		Persona paciente1 ;
		
		Scanner entrada = new Scanner( System.in);
		
		String nombre;
		int edad;
		String sexo;
		double peso;
		double altura;
		double imc;
		
		System.out.print("Nombre de la persona: ");
		nombre = entrada.nextLine();
		//paciente1.setNombre(nombre);
		System.out.print(nombre+"\n");
		System.out.printf("edad: ");
		edad = entrada.nextInt();
		//paciente1.setEdad(edad);
		
		entrada.nextLine();
		
		System.out.printf("Sexo: ");
		sexo = entrada.nextLine();
		//paciente1.setSexo(sexo);
		
		System.out.printf("Peso: ");
		peso = entrada.nextDouble();
		//paciente1.setPeso(peso);
		
		System.out.printf("altura: ");
		altura = entrada.nextDouble();
		//paciente1.setAltura(altura);
		
		altura = altura*2;
		imc = peso / altura;
		System.out.printf("El indice de masa corporal es :%.2f\n",imc);
		
		if ( imc <= peso ){		
			System.out.printf("Falta de peso -1\n");
		}
		if ( imc >= peso ){		
			System.out.printf("Sobre peso 1\n");
		}
		if ( imc == peso) {
			System.out.printf("Peso ideal 0\n");
		}
		
		if ( edad >= 18 ){
			System.out.printf("Mayor de edad");
		}
		
		if ( imc <= 18 ){		
			System.out.printf("Peso bajo. Necesario valorar signos de desnutricion\n");
		}
		else if( (imc >= 18) || (imc <= 24.9)){		
			System.out.printf("\nNormal\n");
		}
		else if( (imc >= 25) || (imc <= 26.9)){		
			System.out.printf("Sobrepeso\n");
		}
		else if ( imc >= 27){		
			System.out.printf("Obesidad\n");
		}
		else if ( (imc >= 27) || (imc <= 29.9)){		
			System.out.printf("Obesidad grado 1.\n");
		}
		else if ( (imc >= 30) || (imc <= 39.9)){		
			System.out.printf("Obesidad grado 2.\n");
		}
		else if ( imc > 40){		
			System.out.printf("Obesidad grado 3 Extrema o Morbida.\n");
		}
		System.out.println(nombre);
		paciente1 = new Persona(nombre, edad, sexo, peso, altura);
		System.out.println(paciente1.getNombre());
		
		
		System.out.print("\n\nEl nombre es : "+paciente1.getNombre());
		System.out.printf("\nLa edad es :%d \n", paciente1.getEdad());
		System.out.printf("El sexo es :%s \n", paciente1.getSexo());
		System.out.printf("El peso es :%.2f \n", paciente1.getPeso());
		System.out.printf("La altura es :%.2f \n", paciente1.getAltura());	
		
	}
}